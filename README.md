# SE G Suite Add-on Project

## Development setup

Clone repository and open it in Intellij. Run ```npm install```. After dependencies are installed 
open terminal in root folder of your project and run ```clasp login``` Clasp should open new browser tab 
asking permissions to access your Google account. Accept and close the tab. In the same terminal run 
```clasp create --type sheets --rootDir=src/```. Clasp will create a new Google Sheet and Script project.
Then in the same terminal run ```clasp push``` to push your files to the created Script project. For more info about Clasp 
read *[here](https://developers.google.com/apps-script/guides/clasp)* and *[here](https://github.com/google/clasp")*.