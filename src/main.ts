import Collaborate from './classes/collaborate';
import Se from "./classes/se";

const FIRM_NAME = 'ca-develop';

function onOpen() {
    SpreadsheetApp.getUi()
        .createMenu('SE')
        .addItem('Import Trial Balance', 'importTB')
        .addToUi();
}

function importTB() {
    if (Collaborate.loggedIn()) {
        //showEntityDialog();
        let cache = CacheService.getUserCache();
        let machineId = cache.get('MachineID');
        getAccounts(machineId);
    } else {
        showLoginDialog();
    }
}

function getAccounts(MachineID: string){
    let accounts = Se.getAccounts(MachineID);
    if (accounts && accounts.objects && accounts.objects.length > 0) {
        let sheet = SpreadsheetApp.getActiveSheet();
        sheet.clear();
        sheet.appendRow(['Account #', 'Account Name', 'Current', 'Prior 1', 'Prior 2', 'Prior 3', 'Prior 4']);
        for (let i = 0; i < accounts.objects.length; i++) {
            sheet.appendRow([
                accounts.objects[i]['number'],
                accounts.objects[i]['name'],
                accounts.objects[i]['balances']['com.caseware.tb.final.0;report;0,0']['amount'],
                accounts.objects[i]['balances']['com.caseware.tb.final.-1;report;-1,0']['amount'],
                accounts.objects[i]['balances']['com.caseware.tb.final.-2;report;-2,0']['amount'],
                accounts.objects[i]['balances']['com.caseware.tb.final.-3;report;-3,0']['amount'],
                accounts.objects[i]['balances']['com.caseware.tb.final.-4;report;-4,0']['amount']
            ]);
        }
    }
}

function login(username: string, password: string) {
    return Collaborate.login(username, password, FIRM_NAME);
}

function showEntityDialog() {
    let html = HtmlService.createTemplateFromFile('interfaces/entity').evaluate()
        .setWidth(450)
        .setHeight(260);
    SpreadsheetApp.getUi().showModalDialog(html, 'CaseWare Cloud');
}

function showLoginDialog() {
    let html = HtmlService.createTemplateFromFile('dialogs/login/page').evaluate()
        .setWidth(250)
        .setHeight(180);
    SpreadsheetApp.getUi().showModalDialog(html, 'CaseWare Cloud');
}

function include(File) {
    return HtmlService.createHtmlOutputFromFile(File).getContent();
}

// export global functions to suppress "unused" IDE warnings
export {onOpen, include, showEntityDialog, login, importTB}
