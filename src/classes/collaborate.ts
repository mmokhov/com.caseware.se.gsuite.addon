import URLFetchRequestOptions = GoogleAppsScript.URL_Fetch.URLFetchRequestOptions;
import Utils from "./utils";
import HTTPResponse = GoogleAppsScript.URL_Fetch.HTTPResponse;

export default class Collaborate {

    static loggedIn() {
        //let cache = CacheService.getUserCache();
        //let uuid = cache.get('UUID');
        //let machineId = cache.get('MachineID');
        return false; //!!(uuid && machineId);
    }

    static getEntities(uuid) {
        let data = {
            'Field': {
                'FieldName': 'Name',
                'Type': 'BaseModel',
                'ExpressionType': 'FilterField'
            },
            'Ascending': true
        };
    }

    static login(username: string, password: string, firmName: string) {
        let loginResult = new LoginResult(Collaborate.request({
            'LoginName': username,
            'Password': password,
            'FirmName': firmName,
            'RememberMe': false,
            'ApplicationType': 'Browser',
            'Language': 'en',
            'RememberTwoFactorAuthentication': false,
            'RetrieveList': []
        }));
        if(!loginResult.error) CacheService.getUserCache().putAll(loginResult);
        return loginResult;
    }

    static request(payload: any): any {

        function parseCollaborateResponse(response: HTTPResponse): any {
            let status = response.getResponseCode();
            if (status === 200) {
                let json = response.getContentText();
                let data = JSON.parse(json);
                let d = data.d || data;
                return d.result || d;
            } else {
                Utils.showError(String(status), response.getContentText())
            }
        }

        let options: URLFetchRequestOptions = {
            'method': 'post',
            'contentType': 'application/json',
            'headers': {'Json-Omit-Nulls': '1'},
            'payload': JSON.stringify(payload)
        };
        let response: HTTPResponse = UrlFetchApp.fetch('https://ca.cwcloudpartner.com/ca-develop/CWCoreService/restricted/SessionService.svc/Login', options);
        Logger.log(response.getAllHeaders());
        return parseCollaborateResponse(response);
    }

}

class LoginResult{
    UUID: string = '';
    MachineID: string ='';
    error: string = '';
    constructor(response: any){
        Logger.log(response);
        if(response['UUID'] && response['MachineID']){
            this.UUID = response['UUID'];
            this.MachineID = response['MachineID'];
        } else if(response && response['Errors'] && response['Errors'].length){
            let errorsText = '';
            response['Errors'].forEach(function(error){
                errorsText+=error.ErrorText + ' ';
            });
            this.error = errorsText;
        }
    }
}

