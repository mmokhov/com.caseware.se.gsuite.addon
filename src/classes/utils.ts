export default class Utils{

    static showError(code: string, message: string) {
        let ui = SpreadsheetApp.getUi();
        ui.alert('Error ' + code + ': ' + message, ui.ButtonSet.OK);
    }

    static showInfo(message: string) {
        let ui = SpreadsheetApp.getUi();
        ui.alert(message, ui.ButtonSet.OK);
    }

}