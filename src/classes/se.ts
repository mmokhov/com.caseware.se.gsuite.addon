
import URLFetchRequestOptions = GoogleAppsScript.URL_Fetch.URLFetchRequestOptions;
import Utils from './utils';

export default class Se {

    static getAccounts(machineId) {
        let data = {
            "jsonrpc": "2.0", "id": 33652, "method": "getAccounts", "params": [{
                "includeTags": true,
                "balances": [{
                    "identifier": "com.caseware.tb.final.0",
                    "raw": false,
                    "specifiers": [{
                        "type": ["report"],
                        "yearOffset": 0,
                        "period": 0,
                        "dimensions": [],
                        "excludeChildren": false,
                        "delta": false
                    }, {
                        "type": ["preliminary"],
                        "yearOffset": 0,
                        "period": 0,
                        "dimensions": [],
                        "excludeChildren": false,
                        "delta": false
                    }, {
                        "type": ["normal_aje", "reclassifying_aje"],
                        "yearOffset": 0,
                        "period": 0,
                        "dimensions": [],
                        "excludeChildren": false,
                        "delta": false
                    }]
                }, {
                    "identifier": "com.caseware.tb.final.-1",
                    "raw": false,
                    "specifiers": [{
                        "type": ["report"],
                        "yearOffset": -1,
                        "period": 0,
                        "dimensions": [],
                        "excludeChildren": false,
                        "delta": false
                    }, {
                        "type": ["preliminary"],
                        "yearOffset": -1,
                        "period": 0,
                        "dimensions": [],
                        "excludeChildren": false,
                        "delta": false
                    }, {
                        "type": ["normal_aje", "reclassifying_aje"],
                        "yearOffset": -1,
                        "period": 0,
                        "dimensions": [],
                        "excludeChildren": false,
                        "delta": false
                    }]
                }, {
                    "identifier": "com.caseware.tax.final.0",
                    "raw": false,
                    "specifiers": [{
                        "type": ["tax_federal_aje", "tax_state_aje", "tax_city_aje", "report"],
                        "yearOffset": 0,
                        "period": 0,
                        "dimensions": [],
                        "excludeChildren": false,
                        "delta": false
                    }, {
                        "type": ["tax_federal_aje", "tax_state_aje", "tax_city_aje"],
                        "yearOffset": 0,
                        "period": 0,
                        "dimensions": [],
                        "excludeChildren": false,
                        "delta": false
                    }]
                }, {
                    "identifier": "com.caseware.tax.final.-1",
                    "raw": false,
                    "specifiers": [{
                        "type": ["tax_federal_aje", "tax_state_aje", "tax_city_aje"],
                        "yearOffset": -1,
                        "period": 0,
                        "dimensions": [],
                        "excludeChildren": false,
                        "delta": false
                    }]
                }, {
                    "identifier": "com.caseware.tb.final.-2",
                    "raw": false,
                    "specifiers": [{
                        "type": ["report"],
                        "yearOffset": -2,
                        "period": 0,
                        "dimensions": [],
                        "excludeChildren": false,
                        "delta": false
                    }]
                }, {
                    "identifier": "com.caseware.tb.final.-3",
                    "raw": false,
                    "specifiers": [{
                        "type": ["report"],
                        "yearOffset": -3,
                        "period": 0,
                        "dimensions": [],
                        "excludeChildren": false,
                        "delta": false
                    }]
                }, {
                    "identifier": "com.caseware.tb.final.-4",
                    "raw": false,
                    "specifiers": [{
                        "type": ["report"],
                        "yearOffset": -4,
                        "period": 0,
                        "dimensions": [],
                        "excludeChildren": false,
                        "delta": false
                    }]
                }, {
                    "identifier": "com.caseware.tb.final.0.del",
                    "raw": false,
                    "specifiers": [{
                        "type": ["report"],
                        "yearOffset": 0,
                        "period": 0,
                        "dimensions": [],
                        "excludeChildren": false,
                        "delta": true
                    }, {
                        "type": ["preliminary"],
                        "yearOffset": 0,
                        "period": 0,
                        "dimensions": [],
                        "excludeChildren": false,
                        "delta": true
                    }, {
                        "type": ["normal_aje", "reclassifying_aje"],
                        "yearOffset": 0,
                        "period": 0,
                        "dimensions": [],
                        "excludeChildren": false,
                        "delta": true
                    }]
                }, {
                    "identifier": "com.caseware.tb.final.-1.del",
                    "raw": false,
                    "specifiers": [{
                        "type": ["report"],
                        "yearOffset": -1,
                        "period": 0,
                        "dimensions": [],
                        "excludeChildren": false,
                        "delta": true
                    }, {
                        "type": ["preliminary"],
                        "yearOffset": -1,
                        "period": 0,
                        "dimensions": [],
                        "excludeChildren": false,
                        "delta": true
                    }, {
                        "type": ["normal_aje", "reclassifying_aje"],
                        "yearOffset": -1,
                        "period": 0,
                        "dimensions": [],
                        "excludeChildren": false,
                        "delta": true
                    }]
                }, {
                    "identifier": "com.caseware.tax.final.0.del",
                    "raw": false,
                    "specifiers": [{
                        "type": ["tax_federal_aje", "tax_state_aje", "tax_city_aje", "report"],
                        "yearOffset": 0,
                        "period": 0,
                        "dimensions": [],
                        "excludeChildren": false,
                        "delta": true
                    }, {
                        "type": ["tax_federal_aje", "tax_state_aje", "tax_city_aje"],
                        "yearOffset": 0,
                        "period": 0,
                        "dimensions": [],
                        "excludeChildren": false,
                        "delta": true
                    }]
                }, {
                    "identifier": "com.caseware.tax.final.-1.del",
                    "raw": false,
                    "specifiers": [{
                        "type": ["tax_federal_aje", "tax_state_aje", "tax_city_aje"],
                        "yearOffset": -1,
                        "period": 0,
                        "dimensions": [],
                        "excludeChildren": false,
                        "delta": true
                    }]
                }, {
                    "identifier": "com.caseware.tb.final.-2.del",
                    "raw": false,
                    "specifiers": [{
                        "type": ["report"],
                        "yearOffset": -2,
                        "period": 0,
                        "dimensions": [],
                        "excludeChildren": false,
                        "delta": true
                    }]
                }, {
                    "identifier": "com.caseware.tb.final.-3.del",
                    "raw": false,
                    "specifiers": [{
                        "type": ["report"],
                        "yearOffset": -3,
                        "period": 0,
                        "dimensions": [],
                        "excludeChildren": false,
                        "delta": true
                    }]
                }, {
                    "identifier": "com.caseware.tb.final.-4.del",
                    "raw": false,
                    "specifiers": [{
                        "type": ["report"],
                        "yearOffset": -4,
                        "period": 0,
                        "dimensions": [],
                        "excludeChildren": false,
                        "delta": true
                    }]
                }]
            }]
        };
        let currentTimeZoneOffsetInMinutes = (new Date()).getTimezoneOffset();
        let cookie = '__machineid__=' + machineId + '; __offset__=' + String(currentTimeZoneOffsetInMinutes / 60);
        let options: URLFetchRequestOptions = {
            'method': 'post',
            'contentType': 'application/json',
            'headers': {
                'X-Timezone-Offset': String(currentTimeZoneOffsetInMinutes),
                'Cookie': cookie
            },
            'payload': JSON.stringify(data)
        };
        try {
            let response = UrlFetchApp.fetch('https://ca.cwcloudpartner.com/ca-develop/e/eng/laFKkm2CQ0efGlJzahkZQA/api/account', options);
            if(response){
                return parseSEResponse(response);
            } else {
                Utils.showError('login error', response.getContentText());
            }
        }catch(e){
            Utils.showError('error', JSON.stringify(e))
        }

    }
}

function parseSEResponse(response){
    let status = response.getResponseCode();
    if (parseInt(status) === 200) {
        let json = response.getContentText();
        let data = JSON.parse(json);
        return data.result || data;
    } else {
        Utils.showError(status, response.getContentText())
    }
}